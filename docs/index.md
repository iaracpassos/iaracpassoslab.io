# Iara Passos

&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; ![](https://lh3.googleusercontent.com/a-/AAuE7mC5Y83NPhAfiDsC4tccrzF3iSzzUU0417KJMfZKIw=s96-cc)

<a href="mailto:iaracpassos@gmail.com"><img src="http://www.newdesignfile.com/postpic/2010/01/simple-email-icon-vector-free_129913.png" width="25" height="25"/></a> <a href="http://gitlab.com/iaracpassos/"><img src="https://about.gitlab.com/images/logos/gitlab-logo.svg" width="20" height="20"/></a>[![](http://i.imgur.com/0o48UoR.png)](https://github.com/ipassos)[![](http://i.imgur.com/tXSoThF.png)](http://www.twitter.com/iara_mouse)<a href="https://orcid.org/0000-0003-2374-8663"><img src="https://ndownloader.figshare.com/files/8439062/preview/8439062/preview.jpg" width="20" height="20"/></a>&nbsp;<a href="https://wwws.cnpq.br/cvlattesweb/PKG_MENU.menu?f_cod=C858415B050D4DE1BF434ABBAA4BD77A#"><img src="http://mmendelson.com/wp-content/uploads/2018/07/lattes-icon.png" width="20" height="20"/></a><a href="https://www.researchgate.net/profile/Iara_Passos"><img src="https://user-images.githubusercontent.com/511683/28757557-f82cff1a-7585-11e7-9317-072a838dcca3.png" width="30" height="35"/></a> <a href="https://scholar.google.com.br/citations?hl=pt-BR&user=4OGJ6qkAAAAJ"><img src="https://img1.gratispng.com/20180503/joq/kisspng-google-scholar-education-research-doctor-of-philos-scholar-s-5aeaddc3812ee6.7249923815253416355291.jpg" width = "25" height="30"/></a>

<span style="font-size:larger;"> **Iara Passos** é bacharel em Ciências Sociais e mestre em Sociologia na Universidade Federal do Rio Grande do Sul. Atualmente cursa o bacharelado em Estatística na mesma universidade. Atua com métodos quantitativos e qualitativos de pesquisa com a linguagem de programação R e os softwares NVivo e SPSS. Tem experiência em pesquisa nas áreas da Sociologia da Violência, TICs, História do Crime e da Polícia, e Sociologia da Ciência e Tecnologia. É fundadora e organizadora do [R-Ladies Porto Alegre](https://www.meetup.com/rladies-porto-alegre/) e ministra cursos de R focados principalmente nas Ciências Humanas. </span>




<span style="font-size: larger;"> Repositório para material de cursos e oficinas. </span>


&nbsp;

<hr/>
<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.
